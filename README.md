Compfest 2014 game.

**how to install**

prereq:

udah nyelesaiin tutorial http://cocos2d-x.org/wiki/How_to_run_cpp-tests_on_Android

** Replace missing folder **

1. ke folder hasil download cocos2d-x

2. open terminal disana

3. ketikkan:

cocos new NusaConqueror -p com.loopstudio.nusacon -l cpp -d loopstudio

4. setelah selesai proses, masuk ke folder loopstudio/NusaConqueror, dan copy folder cocos2d (~200MB) ke repo ini

5. replace Android.mk yang ada pada folder cocos2d/cocos/Android.mk dengan file Android.mk yang ada diroot repo ini



** Eclipse setting **

1. import project dari repo, yang folder proj.android

2. import lagi project dari repo, dalam folder cocos2d/cocos/platform/android/java

3. harusnya ada 2 project di eclipse, "libcocos2dx" dan "NusaConqueror"

4. klik kanan project libcocos2dx > properties > android > centang opsi is library

5. klik kanan project NusaConqueror > properties > android > add > pilih project libcocos2dx > apply > ok

6. klik kanan project NusaConqueror > build path > configure build path > projects > add > pilih project libcocos2dx 

7. sekarang pilih tab order and export, lalu centang folder libcocos2dx bila belum tercentang > OK

8. tunggu compile gak jelas ampe kelar

9. restart eclipse

10. run!