#ifndef  _DATABASE_H_
#define  _DATABASE_H_

#include <cstdio>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "2d/CCAction.h"

#include "external/json/document.h"         // rapidjson's DOM-style API
#include "external/json/prettywriter.h"     // for stringify JSON
#include "external/json/filestream.h"       // wrapper of C stream for prettywriter as output

#include "PrajuritModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

/* Kelas database bertanggung jawab untuk mengambil seluruh data prajurit,item,dll dari file JSON dan menyimpannya.
 */

class Database{
	public:
		static Database& getInstance(){
			static Database instance;
			return instance;
		}
		Database(Database const&);
		void operator=(Database const&);
		rapidjson::Document& getModels();
		rapidjson::Document models;
		PrajuritModel& getModelByName(std::string name);
		//Map<std::string, PrajuritModel> data;
	private:
		Database();
		//static void init();
		//static Database getInstance();
};

#endif // _APP_DELEGATE_H_
