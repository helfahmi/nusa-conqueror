#ifndef  _BENTENG_H_
#define  _BENTENG_H_

#include <iostream>
#include "Player.h"

/* Base merupakan class yang bertanggung jawab untuk melakukan spawn prajurit pada gameplay.
 * responsibility:
 * - listen button click from player
 * - manage cooldowns
 * - manage instances of prajurit that's spawned
 */

class Benteng{
	public:
		Player *p;
		//timer attr? how? liat lib cocos ampas
		Benteng(Player p);

		void spawn(int id);
		void update();
};

#endif
