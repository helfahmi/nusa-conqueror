#ifndef  _PRAJURIT_MODEL_H_
#define  _PRAJURIT_MODEL_H_

#include <iostream>

/* PrajuritModel berisi data-data tentang prajurit tersebut secara keseluruhan,
 * dan PrajuritModel bukan lah kelas yang secara langsung melakukan perang.
 * Untuk kelas yang secara langsung melakukan perang, lihat kelas Prajurit.
 */

class PrajuritModel{
	/* Max limits, diambil dari file database, tidak akan berubah */
	public:
		float atk_range;
		float base_atk, base_hp;
		float atk_speed, walk_speed;
		int price;
		std::string name, image_prefix;
		int modelId;
		std::string modelName;

		PrajuritModel();
};

#endif
