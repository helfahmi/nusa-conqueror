#include "Player.h"
#include "cocos2d.h"

/* Player merupakan kelas yang menyimpan seluruh data player seperti exp player,
 * prajurit-prajurit yang dimiliki player, prajurit-prajurit yang digunakan player,
 * uang player,item player, dll.
 */

class Player{
	Array<Prajurit> prajurits;
	int money;
	float playerExp;
	Array<Item> items;
	Player();
};
