#include "Database.h"

Database::Database(){
	log("Database loaded.");
	ssize_t bufferSize = 0;
	std::string genrefile = FileUtils::sharedFileUtils()->fullPathForFilename("status.json");
	unsigned char * data = FileUtils::sharedFileUtils()->getFileData(genrefile, "rb", &bufferSize );
	const char* json = reinterpret_cast<const char *>(data);

	//const char json[] = " { \"hello\" : \"world\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": 3.1416, \"a\":[1, 2, 3, 4] } ";
	//rapidjson::Document models;      // Default template parameter uses UTF8 and MemoryPoolAllocator.

	if (models.Parse<0>(json).HasParseError()){
		log("ada error di jsonnya");
	} else {
		models.Parse<0>(json);
		//const rapidjson::Value& object = models["article"]["0"][0u]; //<-- hal paling nggak jelas yg pernah gw liat
		//log("string: %s", object.GetString());
		log("content: %s", models.GetString());

		static const char* kTypeNames[] = { "Null", "False", "True", "Object", "Array", "String", "Number" };
		for (rapidjson::Value::ConstMemberIterator itr = models.MemberonBegin(); itr != models.MemberonEnd(); ++itr){
			if(itr){
				log("Type of member %s is %s\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
			}
		}
	}
}

rapidjson::Document& Database::getModels(){
	return this->models;
}

PrajuritModel& Database::getModelByName(std::string name){
	PrajuritModel *model = new PrajuritModel();

	model->name = models[name.c_str()]["name"].GetString();
	model->base_atk = (float) models[name.c_str()]["base_atk"].GetDouble();
	model->base_hp = (float) models[name.c_str()]["base_hp"].GetDouble();
	model->atk_speed = (float) models[name.c_str()]["atk_speed"].GetDouble();
	model->walk_speed = (float) models[name.c_str()]["walk_speed"].GetDouble();
	model->atk_range = (float) models[name.c_str()]["atk_range"].GetDouble();
	model->price = models[name.c_str()]["price"].GetInt();
	model->image_prefix = models[name.c_str()]["image_prefix"].GetString();

	log("---TESTING---");
	log("Nama: %s", model->name.c_str());
	log("Base atk: %.2f", model->base_atk);

	return (*model);
}
