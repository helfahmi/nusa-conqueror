#ifndef  _PRAJURIT_H_
#define  _PRAJURIT_H_

#include <iostream>
#include "PrajuritModel.h"
#include "Database.h"

/* Prajurit merupakan kelas yang melakukan pertarungan sesungguhnya, i.e
 * yang mendapatkan pengurangan HP, yang mendapatkan efek serangan/item,
 * yang mempunyai "wujud fisik". Semua gambar prajurit disetor disini.
 */

class Prajurit:public Node{
	public:
		/* Referensi ke model di database. Hanya akan di-read. */
		PrajuritModel* model;
		/* Max limits, diambil dari file database, tidak akan berubah */
		float cur_exp, cur_atk, cur_hp, next_exp_to_level;

		Sprite* sprite;

		/* Constructor for first time use -- No data saved on this prajurit*/
		Prajurit(PrajuritModel* model);
		/* Constructor for saved prajurit. Load data from json file. */
		Prajurit(PrajuritModel *model, rapidjson::Value& jsonSaveData);
		~Prajurit();

		bool onTouchBegan(Touch* touch, Event* event);
		float getWalkSpeed();
		void setPosition(const Vec2& position);
		void setScale(float scaleX, float scaleY);
		void update(float dt);
};

#endif
