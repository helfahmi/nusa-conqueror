#include "Prajurit.h"

Prajurit::Prajurit(PrajuritModel* model){
	this->model = model;
	cur_exp = 0;
	cur_atk = model->base_atk;
	cur_hp = model->base_hp;

	/* Untuk selanjutnya kalkulasi polinomial / pake rumus */
	next_exp_to_level = 10;

	std::string filename = this->model->image_prefix + ".png";

	sprite = Sprite::create(filename);
	sprite->setVisible(true);
	log("rect_size(sprite): %f, %f", sprite->getContentSize().width, sprite->getContentSize().height);
	this->addChild(sprite);

	// Register Touch Event
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(Prajurit::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, sprite);

	/* TODO: move ke base lawan */
	auto actionBy = MoveBy::create(5, Vec2(-300,0));
	sprite->runAction(actionBy);

	this->scheduleUpdate();
}

bool Prajurit::onTouchBegan(Touch* touch, Event* event)
{
    log("Prajurit::onTouchBegan id = %d, x = %f, y = %f", touch->getID(), touch->getLocation().x, touch->getLocation().y);

    // event->getCurrentTarget() returns the *listener's* sceneGraphPriority node.
	auto target = static_cast<Sprite*>(event->getCurrentTarget());

	//Get the position of the current point relative to the button
	Point locationInNode = target->convertToNodeSpace(touch->getLocation());
	log("locationInNode: x = %f, y = %f", locationInNode.x, locationInNode.y);
	Size s = target->getContentSize();
	Rect rect = Rect(0, 0, s.width, s.height);
	log("rect_size: w = %f, h = %f", s.width, s.height);

	//Check the click area
	if (rect.containsPoint(locationInNode)){
		log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
		target->stopAllActions();
		return true;
	}
	return false;
}

Prajurit::Prajurit(PrajuritModel *model, rapidjson::Value& jsonSaveData){

}

Prajurit::~Prajurit(){
	//delete walk;
}

float Prajurit::getWalkSpeed(){
	return this->model->walk_speed;
}

void Prajurit::setPosition(const Vec2& pos){
	sprite->setPosition(pos);
}

void Prajurit::setScale(float scaleX, float scaleY){
	sprite->setScale(scaleX, scaleY);
}

void Prajurit::update(float dt){
	//if state_walk then use walk sprite
	//else use attack sprite
	//dll.
}
