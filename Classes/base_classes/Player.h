#ifndef  _PLAYER_H_
#define  _PLAYER_H_

#include <iostream>
//#include "Item.h"

/* Player merupakan kelas yang menyimpan seluruh data player seperti exp player,
 * prajurit-prajurit yang dimiliki player, prajurit-prajurit yang digunakan player,
 * uang player,item player, dll.
 */

class Player{
	public:
		//Array<Prajurit> prajurits;
		//Array<Item> items;
		int money;
		float playerExp; //dalam point
		float nextExpToLvlUp; //dalam point
		std::string profileName;

		Player(std::string profileName);
		void save();
		void load();
};

#endif
