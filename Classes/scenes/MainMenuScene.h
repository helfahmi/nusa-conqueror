#ifndef __MAINMENU_SCENE_H__
#define __MAINMENU_SCENE_H__

#include<iostream>
#include<stdio.h>
#include<string>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "editor-support/cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "2d/CCAction.h"

#include "scenes/GameScene.h"
#include "base_classes/Database.h"
#include "base_classes/Prajurit.h"
#include "base_classes/PrajuritModel.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace ui;

class MainMenu : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    void moveToGame(Ref *pSender, Widget::TouchEventType type);
    void touchEvent(Ref *pSender, Widget::TouchEventType type);
    // implement the "static create()" method manually
    CREATE_FUNC(MainMenu);
protected:
    Layout* _widget;
};

#endif // __HELLOWORLD_SCENE_H__
