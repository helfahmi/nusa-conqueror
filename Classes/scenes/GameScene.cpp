#include "GameScene.h"

USING_NS_CC;

Scene* Game::createScene()
{

	// 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

    // 'layer' is an autorelease object
    auto layer = Game::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Game::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    _widget = dynamic_cast<Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("GUI/Game/Game_1.json"));
	Size winSize = Director::getInstance()->getWinSize();
	Size widgetSize = _widget->getContentSize();
	float scaleX = winSize.width/widgetSize.width;
	float scaleY = winSize.height/widgetSize.height;

	float largerScale = (scaleX > scaleY ? scaleX : scaleY);

	_widget->setScale(largerScale, largerScale);
	//Button *start = _widget->getChildByTag(8); //button play dummy (spear)
	//start->addTouchEventListener(CC_CALLBACK_2(MainMenu::moveToGame, this));
	this->addChild(_widget);

    return true;
}

void Game::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    switch (type)
    {
        case Widget::TouchEventType::BEGAN: {
        	//auto actionBy = MoveBy::create(5, Vec2(-300,0));
        	//army->runAction(actionBy);
            //_displayValueLabel->setString(String::createWithFormat("Touch Down")->getCString());
            break;
        }
        case Widget::TouchEventType::MOVED:
            //_displayValueLabel->setString(String::createWithFormat("Touch Move")->getCString());
            break;

        case Widget::TouchEventType::ENDED:
            //_displayValueLabel->setString(String::createWithFormat("Touch Up")->getCString());
            break;

        case Widget::TouchEventType::CANCELED:
           // _displayValueLabel->setString(String::createWithFormat("Touch Cancelled")->getCString());
            break;

        default:
            break;
    }
}

void Game::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
