#include "HelloWorldScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{

	// 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    auto label = LabelTTF::create("Hello World", "Arial", 24);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(label, 1);

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");

    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    // add the sprite as a child to this layer
    this->addChild(sprite, 0);

    auto uButton = ui::Button::create();
    uButton->setTouchEnabled(true);
    uButton->setScale(0.25f, 0.25f);
    uButton->loadTextures("button_lutung.png", "button_lutung2.png", "");
    uButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::touchEvent, this));
	uButton->setPosition(Vec2(200,400));

	this->addChild(uButton, 2);

    return true;
}

void HelloWorld::touchEvent(Ref *pSender, Widget::TouchEventType type)
{
    switch (type)
    {
        case Widget::TouchEventType::BEGAN: {

        	PrajuritModel& pm = Database::getInstance().getModelByName("lutung");
        	auto prajurit = new Prajurit(&pm);
        	std::string filename = prajurit->model->image_prefix + ".png";

        	log("filename: %s", filename.c_str());

        	//Sprite* army = Sprite::create(filename);
        	prajurit->setPosition(Vec2(600, 200));
        	prajurit->setScale(0.25f, 0.25f);
        	this->addChild(prajurit);

        	//auto actionBy = MoveBy::create(5, Vec2(-300,0));
        	//army->runAction(actionBy);
            //_displayValueLabel->setString(String::createWithFormat("Touch Down")->getCString());
            break;
        }
        case Widget::TouchEventType::MOVED:
            //_displayValueLabel->setString(String::createWithFormat("Touch Move")->getCString());
            break;

        case Widget::TouchEventType::ENDED:
            //_displayValueLabel->setString(String::createWithFormat("Touch Up")->getCString());
            break;

        case Widget::TouchEventType::CANCELED:
           // _displayValueLabel->setString(String::createWithFormat("Touch Cancelled")->getCString());
            break;

        default:
            break;
    }
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
